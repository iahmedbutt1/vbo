import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet weak var verticalTableView: UITableView!
    @IBOutlet weak var horizentalTableView: UITableView!
    
    var categories = ["USD", "BTC", "AUD", "BRL", "CAD","CHF","CNY","EUR"]
    override func viewDidLoad() {
        
        //set delegate
        horizentalTableView.delegate = self
        verticalTableView.delegate = self
        
        //set data source
        horizentalTableView.dataSource = self
        verticalTableView.dataSource = self
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.horizentalTableView {
            return 1
        }
        else{
            return categories.count
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
            return 45.0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.horizentalTableView{
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CategoryRow
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1")!
            cell.textLabel?.text = categories[indexPath.row]
        
            return cell
        }
    }

}
